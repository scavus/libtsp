
#include "TspSolverSa.h"

void GenRandomSolution(const NodeArray nodes, NodeArray solution)
{
    U32* shuffle = (U32*)malloc(sizeof(U32) * nodes.length);

    /* Init by random shuffling */
    for (U32 j = 0; j < nodes.length; j++)
    {
        shuffle[j] = j;
    }

    for (S32 j = nodes.length - 1; j >= 0; --j)
    {
        U32 rand_idx = rand() % (j + 1);
        SWAP(shuffle[j], shuffle[rand_idx], U32);
    }

    Node* chr_data = solution.data;

    for (U32 j = 0; j < nodes.length; j++)
    {
        chr_data[j] = nodes.data[shuffle[j]];
    }

    free(shuffle);
}

void GenProposal(const NodeArray current_solution, NodeArray proposed_solution)
{
    NodeArrayCopy(&proposed_solution, current_solution);

    U32 a0 = rand() % proposed_solution.length;
    U32 a1 = rand() % proposed_solution.length;

    while (a0 == a1)
    {
        a1 = rand() % proposed_solution.length;
    }

    if (a0 > a1)
    {
        SWAP(a0, a1, U32);
    }

    SWAP(proposed_solution.data[a0], proposed_solution.data[a1], Node);
}

void SolveTspSa(const NodeArray nodes, const TspSolverSaSettings* settings_ptr, TspSolverSaStatistics* statistics_ptr)
{
    NodeArray current_solution = NodeArrayCreate(nodes.length);
    NodeArray proposed_solution = NodeArrayCreate(nodes.length);
    NodeArray best_solution = NodeArrayCreate(nodes.length);
    S32 best_fitness = INT32_MIN;
    F32 avg_fitness = 0.0f;

    GenRandomSolution(nodes, current_solution);
    best_fitness = Fitness(current_solution);
    avg_fitness += (F32)best_fitness;

    U32 total_it = 0;

    F32 temp = settings_ptr->initial_temp;

    clock_t t_start = clock();
    while ( (temp > settings_ptr->final_temp) && ((F32)(clock() - t_start) / CLOCKS_PER_SEC < settings_ptr->max_runtime) )
    {
        for (U32 i = 0; i < settings_ptr->iteration_count; i++)
        {
            GenProposal(current_solution, proposed_solution);

            F32 delta = (F32)Fitness(proposed_solution) - (F32)Fitness(current_solution);

            F32 a = fminf(1.0f, expf(delta / temp));

            if (RAND_F32() < a)
            {
                NodeArrayCopy(&current_solution, proposed_solution);
                S32 current_fitness = Fitness(current_solution);

                if (current_fitness > best_fitness)
                {
                    best_fitness = current_fitness;
                }
            }

            total_it++;

            avg_fitness += (F32)Fitness(current_solution);
        }

        temp *= settings_ptr->cooling_rate;
    }
    clock_t t_end = clock();

    statistics_ptr->best_result = best_fitness;
    statistics_ptr->avg_result = avg_fitness / (F32)total_it;
    statistics_ptr->runtime = (F32)(t_end - t_start) / CLOCKS_PER_SEC;
    statistics_ptr->total_it = total_it;

    NodeArrayDestroy(&current_solution);
    NodeArrayDestroy(&proposed_solution);
    NodeArrayDestroy(&best_solution);
}
