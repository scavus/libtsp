
#include "TspCommon.h"
#include "TspSeeds.h"
#include "TspNodeArray.h"
#include "TspPopulation.h"
#include "TspOperators.h"
#include "TspSolverGaDyna.h"
#include "TspPlotting.h"

#include "julia.h"

void Experiment1(const char* node_filename, const U32 node_count)
{
    NodeArray nodes = LoadNodes(node_filename, node_count);

    char output_filename[100] = "exp1.";
    strcat(output_filename, node_filename);
    strcat(output_filename, ".csv");

    const U32 seed_count = 10;

    U32 traffic_update_freqs[] = { 20, 50, 100 };
    U32 traffic_update_freq_count = 3;
    F32 traffic_add_probs[] = { 0.1f, 0.25f, 0.5f, 0.75f, 1.0f };
    U32 traffic_add_prob_count = 5;
    FnSolveDynaTsp solver_fns[] = { SolveDynaTspGaSteadyState, SolveDynaTspGaRandomImmigrants, SolveDynaTspGaMemorySearch };
    char* solver_fn_names[] = { "SS", "RI", "MS" };
    U32 solver_fn_count = 3;

    TspSolverGaDynaSettings settings;
    settings.population_size = 50;
    settings.tournament_size = 5;
    settings.max_generation_count = 1000;
    settings.max_runtime = FLT_MAX;
    settings.crossover_fn = CrossoverPmx;
    settings.mutate_fn = MutateIvm;
    settings.mutation_prob = 0.1f;

    DynaPolicySettings dyna_policy_settings;
    dyna_policy_settings.replacement_rate = 30;
    settings.dyna_policy_settings = dyna_policy_settings;

    FILE* exp1_csv = fopen(output_filename, "w+");
    fprintf(exp1_csv, "Algorithm,Traffic,,,,\n");

    for (U32 f = 0; f < traffic_update_freq_count; f++)
    {
        settings.traffic_update_freq = traffic_update_freqs[f];
        fprintf(exp1_csv, "$f=%d$,$m=0.1$,$m=0.25$,$m=0.5$,$m=0.75$,$m=1.0$\n", settings.traffic_update_freq);

        for (U32 a = 0; a < solver_fn_count; a++)
        {
            FnSolveDynaTsp solver_fn = solver_fns[a];
            fprintf(exp1_csv, "%s", solver_fn_names[a]);

            for (U32 m = 0; m < traffic_add_prob_count; m++)
            {
                F32 avg_perf = 0.0f;
                const F32 w = 1.0f / (F32)seed_count;
                settings.traffic_add_prob = traffic_add_probs[m];

                for (U32 s = 0; s < seed_count; s++)
                {
                    srand(RNG_SEEDS_100[s]);
                    TspSolverGaDynaStatistics statistics;
                    statistics.perf_buffer = NULL;
                    (*solver_fn)(nodes, &settings, &statistics);
                    avg_perf += w * statistics.perf;
                }

                fprintf(exp1_csv, ",%.2f", avg_perf);
            }

            fprintf(exp1_csv, "\n");
        }

        fprintf(exp1_csv, ",,,,,\n");
    }

    fclose(exp1_csv);

    NodeArrayDestroy(&nodes);
}

void Experiment2(const char* node_filename, const U32 node_count)
{
    jl_init();

    jl_eval_string(script);
    jl_function_t* plot_fn = jl_get_function(jl_main_module, "plot_dyna");

    NodeArray nodes = LoadNodes(node_filename, node_count);

    const U32 seed_count = 1;

    U32 traffic_update_freqs[] = { 20, 100 };
    U32 traffic_update_freq_count = 2;
    F32 traffic_add_probs[] = { 0.1f, 0.5f, 1.0f };
    U32 traffic_add_prob_count = 3;
    FnSolveDynaTsp solver_fns[] = { SolveDynaTspGaSteadyState, SolveDynaTspGaRandomImmigrants, SolveDynaTspGaMemorySearch };
    char* solver_fn_names[] = { "SS", "RI", "MS" };
    U32 solver_fn_count = 3;

    TspSolverGaDynaSettings settings;
    settings.population_size = 50;
    settings.tournament_size = 5;
    settings.max_generation_count = 1000;
    settings.max_runtime = FLT_MAX;
    settings.crossover_fn = CrossoverPmx;
    settings.mutate_fn = MutateIvm;
    settings.mutation_prob = 0.1f;

    DynaPolicySettings dyna_policy_settings;
    dyna_policy_settings.replacement_rate = 10;
    settings.dyna_policy_settings = dyna_policy_settings;

    jl_array_t* perf_buffers[] = { NULL, NULL, NULL };
    jl_value_t* array_type_f32 = jl_apply_array_type((jl_value_t*)jl_float32_type, 1);
    jl_value_t* array_type_s32 = jl_apply_array_type((jl_value_t*)jl_int32_type, 1);
    perf_buffers[0] = jl_alloc_array_1d(array_type_f32, settings.max_generation_count);
    perf_buffers[1] = jl_alloc_array_1d(array_type_f32, settings.max_generation_count);
    perf_buffers[2] = jl_alloc_array_1d(array_type_f32, settings.max_generation_count);

    for (U32 f = 0; f < traffic_update_freq_count; f++)
    {
        settings.traffic_update_freq = traffic_update_freqs[f];

        for (U32 m = 0; m < traffic_add_prob_count; m++)
        {
            settings.traffic_add_prob = traffic_add_probs[m];

            for (U32 a = 0; a < solver_fn_count; a++)
            {
                FnSolveDynaTsp solver_fn = solver_fns[a];

                TspSolverGaDynaStatistics statistics;
                statistics.perf_buffer = (F32*)jl_array_data(perf_buffers[a]);
                srand(RNG_SEEDS_100[0]);
                (*solver_fn)(nodes, &settings, &statistics);
            }

            jl_value_t* args[] =
            {
                jl_box_uint32(settings.traffic_update_freq),
                jl_box_int32(nearbyintf(settings.traffic_add_prob * 100.0f)),
                jl_pchar_to_string(node_filename, strlen(node_filename)),
                (jl_value_t*)perf_buffers[0],
                (jl_value_t*)perf_buffers[1],
                (jl_value_t*)perf_buffers[2]
            };
            jl_call(plot_fn, args, 6);
        }

    }

    NodeArrayDestroy(&nodes);

    jl_atexit_hook(0);
}

int main(int argc, char const *argv[])
{
    Experiment1("eil76.tsp", 76);
    // Experiment1("kroB200.tsp", 200);

    //Experiment2("eil76.tsp", 76);
    // Experiment2("kroB200.tsp", 200);

    return 0;
}
