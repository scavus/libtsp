
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE( "OX Crossover" )
{
    const S32 p0_data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    const S32 p1_data[] = { 9, 3, 7, 8, 2, 6, 5, 1, 4 };
    const S32 ref_offspring_data[] = { 3, 8, 2, 4, 5, 6, 7, 1, 9 };

    Chromosome p0 = NodeArrayCreate(9);
    Chromosome p1 = NodeArrayCreate(9);

    Chromosome offspring = NodeArrayCreate(9);
    Chromosome ref_offspring = NodeArrayCreate(9);

    for (U32 i = 0; i < p0.length; i++)
    {
        p0.data[i].id = p0_data[i];
        p1.data[i].id = p1_data[i];
        ref_offspring.data[i].id = ref_offspring_data[i];
    }

    const U32 a0 = 3;
    const U32 a1 = 6;

    _CrossoverOx(a0, a1, p0, p1, offspring);

    REQUIRE( NodeArrayIsEqual(offspring, ref_offspring) );

    NodeArrayDestroy(&p0);
    NodeArrayDestroy(&p1);
    NodeArrayDestroy(&offspring);
    NodeArrayDestroy(&ref_offspring);
}

TEST_CASE("PMX Crossover")
{
    const S32 p0_data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    const S32 p1_data[] = { 9, 3, 7, 8, 2, 6, 5, 1, 4 };
    const S32 ref_offspring_data[] = { 9, 3, 2, 4, 5, 6, 7, 1, 8 };

    Chromosome p0 = NodeArrayCreate(9);
    Chromosome p1 = NodeArrayCreate(9);

    Chromosome offspring = NodeArrayCreate(9);
    Chromosome ref_offspring = NodeArrayCreate(9);

    for (U32 i = 0; i < p0.length; i++)
    {
        p0.data[i].id = p0_data[i];
        p1.data[i].id = p1_data[i];
        ref_offspring.data[i].id = ref_offspring_data[i];
    }

    const U32 a0 = 3;
    const U32 a1 = 6;

    _CrossoverPmx(a0, a1, p0, p1, offspring);

    REQUIRE(NodeArrayIsEqual(offspring, ref_offspring));

    NodeArrayDestroy(&p0);
    NodeArrayDestroy(&p1);
    NodeArrayDestroy(&offspring);
    NodeArrayDestroy(&ref_offspring);
}



TEST_CASE("IVM Mutation")
{

}

TEST_CASE("SM Mutation")
{

}

TEST_CASE("Population creation")
{
    NodeArray nodes = LoadNodes("kroA100.tsp");

    for (U32 n = 0; n < 1000; n++)
    {
        srand(time(NULL));
        Population population = PopulationCreate(50, nodes);

        for (U32 i = 0; i < 50; i++)
        {
            REQUIRE( NodeArrayHasDuplicate(population.chromosomes[i]) == false );
        }

        PopulationDestroy(&population);
    }

    NodeArrayDestroy(&nodes);
}
