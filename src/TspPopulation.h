
#pragma once

#include "TspCommon.h"
#include "TspNodeArray.h"

typedef struct Population
{
    Chromosome* chromosomes;
    U32 size;
    U32 capacity;

} Population;

inline bool PopulationIsFull(const Population population)
{
    if (population.size < population.capacity)
    {
        return false;
    }
    else
    {
        return true;
    }
}

Population PopulationAlloc(const U32 size, const NodeArray nodes);
Population PopulationCreate(const U32 size, const NodeArray nodes);
void PopulationDestroy(Population* population_ptr);
void PopulationSortByFitnessDescending(Population* population_ptr, const TrafficMtx* traffic_ptr);
U32 PopulationGetBestIdx(const Population population, const TrafficMtx* traffic_ptr);
U32 PopulationGetWorstIdx(const Population population, const TrafficMtx* traffic_ptr);
F32 PopulationComputeAvgFitness(const Population population, const TrafficMtx* traffic_ptr);
U32 PopulationSampleIdxTournament(const Population population, const U32 tournament_size, const TrafficMtx* traffic_ptr);
U32 PopulationSampleIdxMinDist(const Population population, const TrafficMtx* traffic_ptr);
