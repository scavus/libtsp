
#pragma once

#include "TspCommon.h"
#include "TspNodeArray.h"
#include "TspPopulation.h"
#include "TspOperators.h"

typedef struct TspSolverGaDynaStatistics
{
    S32 best_result;
    F32 avg_result;
    F32 perf;
    F32* perf_buffer;

} TspSolverGaDynaStatistics;

typedef struct DynaPolicySettings
{
    U32 replacement_rate;

} DynaPolicySettings;

typedef void (*FnDynaPolicy)(const DynaPolicySettings settings, const NodeArray nodes, Population* population_ptr);

typedef struct TspSolverGaDynaSettings
{
    U32 population_size;
    U32 tournament_size;
    U32 max_generation_count;
    F32 mutation_prob;
    F32 max_runtime;

    U32 traffic_update_freq;
    F32 traffic_add_prob;

    FnCrossover crossover_fn;
    FnMutate mutate_fn;

    DynaPolicySettings dyna_policy_settings;

} TspSolverGaDynaSettings;

void SolveDynaTspGaSteadyState(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr);
void SolveDynaTspGaRandomImmigrants(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr);
void SolveDynaTspGaMemorySearch(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr);

typedef void (*FnSolveDynaTsp)(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr);
