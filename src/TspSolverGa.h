
#pragma once

#include "TspCommon.h"
#include "TspNodeArray.h"

typedef struct TspSolverGaStatistics
{
    S32 best_result;
    F32 avg_result;

} TspSolverGaStatistics;

typedef struct TspSolverGaSettings
{
    U32 population_size;
    U32 tournament_size;
    U32 max_generation_count;
    F32 mutation_prob;
    U32 k;
    U32 m;
    U32 n;
    F32 max_runtime;

} TspSolverGaSettings;

typedef void (*FnCrossover)(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1);
typedef void (*FnMutate)(NodeArray* chr);

void SolveTspGa(const NodeArray nodes, const TspSolverGaSettings* settings_ptr, const FnCrossover crossover_fn, const FnMutate mutate_fn, TspSolverGaStatistics* statistics_ptr);
