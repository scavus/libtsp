
#include "TspOperators.h"

void _CrossoverPmx(const U32 a0, const U32 a1, const NodeArray parent0, const NodeArray parent1, NodeArray offspring0)
{
    for (U32 i = a0; i <= a1; i++)
    {
        offspring0.data[i] = parent0.data[i];
    }

    U32 empty_count = offspring0.length - (a1 - a0 + 1);

    for (U32 i = a0; i <= a1; i++)
    {
        Node p1_node = parent1.data[i];

        if (!NodeArrayContains(offspring0, p1_node))
        {
            U32 target_idx = i;

            while (offspring0.data[target_idx].id != 0)
            {
                target_idx = NodeArrayFindIdx(parent1, offspring0.data[target_idx]);
            }

            offspring0.data[target_idx] = p1_node;
            empty_count--;
        }
    }

    U32 empty_slots_it = 0;
    U32* empty_slots = (U32*)malloc(sizeof(U32) * empty_count);

    /* Find empty slots */
    {
        U32 target_idx = WRAP(a1 + 1, offspring0.length);

        while (empty_slots_it < empty_count)
        {
            if (offspring0.data[target_idx].id == 0)
            {
                empty_slots[empty_slots_it++] = target_idx;
            }

            target_idx = WRAP(target_idx + 1, offspring0.length);
        }

        empty_slots_it = 0;
    }

    /* Fill empty slots */
    {
        U32 src_idx = WRAP(a1 + 1, parent1.length);

        while (empty_slots_it < empty_count)
        {
            if (!NodeArrayContains(offspring0, parent1.data[src_idx]))
            {
                U32 target_idx = empty_slots[empty_slots_it++];
                offspring0.data[target_idx] = parent1.data[src_idx];
            }

            src_idx = WRAP(src_idx + 1, parent1.length);
        }
    }

    free(empty_slots);
}

/* Partially Mapped Crossover */
void CrossoverPmx(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1)
{
    U32 a0 = rand() % parent0.length;
    U32 a1 = rand() % parent0.length;

    while (a0 == a1)
    {
        a1 = rand() % parent0.length;
    }

    if (a0 > a1)
    {
        SWAP(a0, a1, U32);
    }

    _CrossoverPmx(a0, a1, parent0, parent1, offspring0);
    _CrossoverPmx(a0, a1, parent1, parent0, offspring1);
}

void _CrossoverOx(const U32 a0, const U32 a1, const NodeArray parent0, const NodeArray parent1, NodeArray offspring0)
{
    for (U32 i = a0; i <= a1; i++)
    {
        offspring0.data[i] = parent0.data[i];
    }

    U32 empty_count = offspring0.length - (a1 - a0 + 1);
    U32 empty_slots_it = 0;
    U32* empty_slots = (U32*)malloc(sizeof(U32) * empty_count);

    /* Find empty slots */
    {
        U32 target_idx = WRAP(a1 + 1, offspring0.length);

        while (empty_slots_it < empty_count)
        {
            if (offspring0.data[target_idx].id == 0)
            {
                empty_slots[empty_slots_it++] = target_idx;
            }

            target_idx = WRAP(target_idx + 1, offspring0.length);
        }

        empty_slots_it = 0;
    }

    /* Fill empty slots */
    {
        U32 src_idx = WRAP(a1 + 1, parent1.length);

        while (empty_slots_it < empty_count)
        {
            if (!NodeArrayContains(offspring0, parent1.data[src_idx]))
            {
                U32 target_idx = empty_slots[empty_slots_it++];
                offspring0.data[target_idx] = parent1.data[src_idx];
            }

            src_idx = WRAP(src_idx + 1, parent1.length);
        }
    }

    free(empty_slots);
}

/* Order Crossover */
void CrossoverOx(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1)
{
    U32 a0 = rand() % parent0.length;
    U32 a1 = rand() % parent0.length;

    while (a0 == a1)
    {
        a1 = rand() % parent0.length;
    }

    if (a0 > a1)
    {
        SWAP(a0, a1, U32);
    }

    _CrossoverOx(a0, a1, parent0, parent1, offspring0);
    _CrossoverOx(a0, a1, parent1, parent0, offspring1);
}

void _CrossoverPbx(const U32* pos_set, const U32 pos_set_length, const NodeArray parent0, const NodeArray parent1, NodeArray offspring0)
{
    for (U32 i = 0; i < pos_set_length; i++)
    {
        U32 pos = pos_set[i];
        offspring0.data[pos] = parent0.data[pos];
    }

    U32 parent1_it = 0;

    for (U32 i = 0; i < offspring0.length; i++)
    {
        if (offspring0.data[i].id != 0)
        {
            continue;
        }

        Node src_node = parent1.data[parent1_it++];

        while (NodeArrayContains(offspring0, src_node))
        {
            src_node = parent1.data[parent1_it++];
        }

        offspring0.data[i] = src_node;
    }
}

/* Point Based Crossover */
void CrossoverPbx(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1)
{
    U32 pos_set_length = (rand() % parent0.length) + 1;
    U32* pos_set = (U32*)malloc(pos_set_length * sizeof(U32));

    for (U32 i = 0; i < pos_set_length; i++)
    {
        U32 pos = UINT32_MAX;

        bool contains = true;

        while (contains)
        {
            contains = false;
            pos = rand() % parent0.length;

            for (U32 j = 0; j < i; j++)
            {
                if (pos_set[j] == pos)
                {
                    contains = true;
                }
            }
        }

        pos_set[i] = pos;
    }

    _CrossoverPbx(pos_set, pos_set_length, parent0, parent1, offspring0);
    _CrossoverPbx(pos_set, pos_set_length, parent1, parent0, offspring1);

    free(pos_set);
}

/* Swap Mutation */
void MutateSm(NodeArray* chr)
{
    U32 a0 = rand() % chr->length;
    U32 a1 = rand() % chr->length;

    while (a0 == a1)
    {
        a1 = rand() % chr->length;
    }

    if (a0 > a1)
    {
        SWAP(a0, a1, U32);
    }

    SWAP(chr->data[a0], chr->data[a1], Node);
}

/* Inverse Mutation */
void MutateIvm(NodeArray* chr)
{
    U32 a0 = rand() % chr->length;
    U32 a1 = rand() % chr->length;

    while (a0 == a1)
    {
        a1 = rand() % chr->length;
    }

    if (a0 > a1)
    {
        SWAP(a0, a1, U32);
    }

    a0++;
    a1--;

    while (a0 < a1)
    {
        SWAP(chr->data[a0], chr->data[a1], Node);

        a0++;
        a1--;
    }
}

void ExchangeHeuristic2Opt(Chromosome* chr_ptr)
{
    U32 x0 = rand() % chr_ptr->length;
    U32 x1 = WRAP(x0 + 1, chr_ptr->length);

    U32 y0 = rand() % chr_ptr->length;

    /* Avoid duplicate edges */
    while (y0 == x0 || y0 == x1)
    {
        y0 = rand() % chr_ptr->length;
    }

    U32 y1 = WRAP(y0 + 1, chr_ptr->length);

    SWAP(chr_ptr->data[x1], chr_ptr->data[y0], Node);
}
