
#pragma once

#include "TspCommon.h"
#include "TspNodeArray.h"

typedef struct TspSolverSaStatistics
{
    S32 best_result;
    F32 avg_result;
    U32 total_it;
    F32 runtime;

} TspSolverSaStatistics;

typedef struct TspSolverSaSettings
{
    F32 initial_temp;
    F32 cooling_rate;
    U32 iteration_count;
    F32 final_temp;
    F32 max_runtime;

} TspSolverSaSettings;

void SolveTspSa(const NodeArray nodes, const TspSolverSaSettings* settings_ptr, TspSolverSaStatistics* statistics_ptr);
