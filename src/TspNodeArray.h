
#pragma once

#include "TspCommon.h"

typedef struct Node
{
    S32 id;
    S32 x;
    S32 y;
} Node;

typedef struct NodeArray
{
    Node* data;
    U32 length;

} NodeArray;

typedef NodeArray Chromosome;

NodeArray NodeArrayCreate(const U32 length);
void NodeArrayCopy(NodeArray* target, const NodeArray src);
bool NodeArrayIsEqual(const NodeArray a, const NodeArray b);
bool NodeArrayContains(const NodeArray nodes, const Node node);
bool NodeArrayHasDuplicate(const NodeArray nodes);
U32 NodeArrayFindIdx(const NodeArray nodes, const Node node);
void NodeArrayDestroy(NodeArray* node_arr_ptr);
NodeArray LoadNodes(const char* filename, const U32 node_count);


typedef struct TrafficMtx
{
    F32* data;
    U32 row_count;
    U32 col_count;
} TrafficMtx;

TrafficMtx TrafficMtxCreate(const U32 row_count, const U32 col_count);
void TrafficMtxDestroy(TrafficMtx* mtx_ptr);
void TrafficMtxUpdate(TrafficMtx* mtx_ptr, const F32 traffic_add_prob);

inline F32 TrafficMtxGet(const TrafficMtx mtx, const U32 i, const U32 j)
{
    return mtx.data[mtx.col_count * i + j];
}

inline void TrafficMtxSet(TrafficMtx* mtx_ptr, const U32 i, const U32 j, const F32 f)
{
    mtx_ptr->data[mtx_ptr->col_count * i + j] = f;
}


S32 Distance(const Node* n0_ptr, const Node* n1_ptr);
S32 Fitness(const NodeArray chr);
S32 FitnessWeighted(const NodeArray chr, const TrafficMtx* traffic_ptr);
