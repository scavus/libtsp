
#pragma once

#include "TspCommon.h"
#include "TspNodeArray.h"

void CrossoverPmx(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1);
void CrossoverOx(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1);
void CrossoverPbx(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1);

void MutateSm(NodeArray* chr);
void MutateIvm(NodeArray* chr);

void ExchangeHeuristic2Opt(Chromosome* chr_ptr);

typedef void (*FnCrossover)(const NodeArray parent0, const NodeArray parent1, NodeArray offspring0, NodeArray offspring1);
typedef void (*FnMutate)(NodeArray* chr);
