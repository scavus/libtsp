
#pragma once

static const char* script = "using PyPlot\n"
"PyPlot.matplotlib[:rc](\"text\", usetex=true)\n"
"rc(\"text.latex\", preamble=\"\\\\usepackage{fourier}\")\n"
"plt[:style][:use](\"bmh\")\n"
"function plot_dyna(f, m, b, perf_ss, perf_ri, perf_ms)\n"
    "lw = 1.25\n"
    "figure()\n"
    "title(latexstring( \"\\$f=\" * string(f) * \",\" * \"m=\" * string(m / 100) * \"\\$\"))\n"
    "plot(perf_ss, linewidth=lw, label=L\"SS\")\n"
    "plot(perf_ri, linewidth=lw, label=L\"RI\")\n"
    "plot(perf_ms, linewidth=lw, label=L\"MS\")\n"
    "xlabel(latexstring(\"Generations\"))\n"
    "legend()\n"
    "savefig(\"perf-\" * \"f\" * string(f) * \"-m\" * string(m) * \"-\" * b * \".pdf\")\n"
"end\n";
