
set (Tsp_SRCS
    TspNodeArray.c
    TspPopulation.c
    TspOperators.c
    TspSolverGa.c
    TspSolverGaDyna.c
    TspSolverSa.c
    TspExperiment.c
)

add_executable(ga ${Tsp_SRCS})
target_link_libraries(ga ${JULIA_LIBRARIES} m)
