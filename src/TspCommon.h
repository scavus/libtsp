
#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdbool.h>
#include <float.h>
#include <time.h>

typedef uint8_t U8;
typedef int32_t S32;
typedef uint32_t U32;
typedef float F32;
typedef double F64;

#define NODE_COUNT 100

static const U32 REFERENCE_SOL[] = {
1, 47, 93, 28, 67, 58, 61, 51, 87, 25, 81, 69, 64,
40, 54, 2, 44, 50, 73, 68, 85, 82, 95, 13, 76, 33,
37, 5, 52, 78, 96, 39, 30, 48, 100, 41, 71, 14, 3,
43, 46, 29, 34, 83, 55, 7, 9, 57, 20, 12, 27, 86, 35,
62, 60, 77, 23, 98, 91, 45, 32, 11, 15, 17, 59, 74, 21,
72,10, 84, 36, 99, 38, 24, 18, 79, 53, 88, 16, 94, 22,
70, 66, 26, 65, 4, 97, 56, 80, 31, 89, 42, 8, 92, 75,
19, 90, 49, 6, 63};

#define WRAP(i, length) (i == length) ? 0 : i
#define SWAP(x, y, T) T tmp = x; x = y; y = tmp
#define RAND_F32() (F32)rand() / (F32)RAND_MAX
