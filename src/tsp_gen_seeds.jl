
using StatsBase
seed_count = 100
seeds = sample(1:typemax(Int32), seed_count, replace=false)

f = open("TspSeeds.h", "w")
write(f, "#pragma once\n")
write(f, "#include \"TspCommon.h\"\n")
write(f, "static const S32 RNG_SEEDS_100[] = {\n")

for s = seeds
    write(f, string(s), ",\n")
end

write(f, "};\n")
write(f, "static const U32 RNG_SEED_COUNT = $seed_count;")
close(f)
