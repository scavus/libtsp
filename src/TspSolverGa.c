
#include "TspSolverGa.h"
#include "TspPopulation.h"

void SolveTspGa(const NodeArray nodes, const TspSolverGaSettings* settings_ptr, const FnCrossover crossover_fn, const FnMutate mutate_fn, TspSolverGaStatistics* statistics_ptr)
{
    Population population = PopulationCreate(settings_ptr->population_size, nodes);

    clock_t t_start = clock();

    for (U32 i = 0; i < settings_ptr->max_generation_count; i++)
    {
        U32 parent0_idx = PopulationSampleIdxTournament(population, settings_ptr->tournament_size);
        U32 parent1_idx = PopulationSampleIdxTournament(population, settings_ptr->tournament_size);

        while (parent0_idx == parent1_idx)
        {
            parent1_idx = PopulationSampleIdxTournament(population, settings_ptr->tournament_size);
        }

        const Chromosome parent0 = population.chromosomes[parent0_idx];
        const Chromosome parent1 = population.chromosomes[parent1_idx];

        Chromosome offspring0 = NodeArrayCreate(nodes.length);
        Chromosome offspring1 = NodeArrayCreate(nodes.length);
        (*crossover_fn)(parent0, parent1, offspring0, offspring1);

        if (RAND_F32() < settings_ptr->mutation_prob)
        {
            (*mutate_fn)(&offspring0);
            (*mutate_fn)(&offspring1);
        }

        const U32 worst_chr_idx_1 = PopulationGetWorstIdx(population);
        NodeArrayCopy(&population.chromosomes[worst_chr_idx_1], offspring0);

        const U32 worst_chr_idx_2 = PopulationGetWorstIdx(population);
        NodeArrayCopy(&population.chromosomes[worst_chr_idx_2], offspring1);

        /* Local search by 2-opt
        if (i % settings_ptr->k == 0)
        {
            {
                Chromosome new_chr = NodeArrayCreate(nodes.length);
                const U32 best_chr_idx = PopulationGetBestIdx(population);
                Chromosome best_chr = population.chromosomes[best_chr_idx];
                NodeArrayCopy(&new_chr, best_chr);

                for (U32 n = 0; n < settings_ptr->n; n++)
                {
                    ExchangeHeuristic2Opt(&new_chr);

                    if (Fitness(new_chr) > Fitness(best_chr))
                    {
                        NodeArrayCopy(&best_chr, new_chr);
                    }
                }

                NodeArrayDestroy(&new_chr);
            }

            for (U32 m = 0; m < settings_ptr->m; m++)
            {
                Chromosome new_chr = NodeArrayCreate(nodes.length);
                const U32 rand_chr_idx = rand() % population.size;
                Chromosome rand_chr = population.chromosomes[rand_chr_idx];
                NodeArrayCopy(&new_chr, rand_chr);

                for (U32 n = 0; n < settings_ptr->n; n++)
                {
                    ExchangeHeuristic2Opt(&new_chr);

                    if (Fitness(new_chr) > Fitness(rand_chr))
                    {
                        NodeArrayCopy(&rand_chr, new_chr);
                    }
                }

                NodeArrayDestroy(&new_chr);
            }
        }
        */

        NodeArrayDestroy(&offspring0);
        NodeArrayDestroy(&offspring1);

        U32 worst_idx = PopulationGetWorstIdx(population);
        U32 best_idx = PopulationGetBestIdx(population);
        statistics_ptr->best_result = Fitness(population.chromosomes[best_idx]);
        statistics_ptr->avg_result = PopulationComputeAvgFitness(population);

        if ((F32)(clock() - t_start) / CLOCKS_PER_SEC >= settings_ptr->max_runtime)
        {
            break;
        }
    }

    PopulationDestroy(&population);
}
