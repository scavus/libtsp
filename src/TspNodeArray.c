
#include "TspNodeArray.h"

NodeArray NodeArrayCreate(const U32 length)
{
    NodeArray node_arr;
    node_arr.data = (Node*)calloc(length, sizeof(Node));
    node_arr.length = length;
    return node_arr;
}

void NodeArrayCopy(NodeArray* target, const NodeArray src)
{
    for (U32 i = 0; i < src.length; i++)
    {
        target->data[i] = src.data[i];
    }
}

bool NodeArrayIsEqual(const NodeArray a, const NodeArray b)
{
    bool equal_flag = true;

    for (U32 i = 0; i < a.length; i++)
    {
        if (a.data[i].id != b.data[i].id)
        {
            equal_flag = false;
            break;
        }
    }

    return equal_flag;
}

bool NodeArrayContains(const NodeArray nodes, const Node node)
{
    bool contains_flag = false;

    for (U32 i = 0; i < nodes.length; i++)
    {
        if (nodes.data[i].id == node.id)
        {
            contains_flag = true;
            break;
        }
    }

    return contains_flag;
}

bool NodeArrayHasDuplicate(const NodeArray nodes)
{
    bool has_duplicate = false;

    U8* slots = (U8*)calloc(nodes.length, sizeof(U8));

    for (U32 i = 0; i < nodes.length; i++)
    {
        U32 id = nodes.data[i].id;

        if (slots[id - 1] != 0)
        {
            has_duplicate = true;
            break;
        }
        else
        {
            slots[id - 1] = 1;
        }
    }

    free(slots);

    return has_duplicate;
}

U32 NodeArrayFindIdx(const NodeArray nodes, const Node node)
{
    for (U32 i = 0; i < nodes.length; i++)
    {
        if(nodes.data[i].id == node.id)
        {
            return i;
        }
    }

    return UINT32_MAX;
}

void NodeArrayDestroy(NodeArray* node_arr_ptr)
{
    free(node_arr_ptr->data);
    node_arr_ptr->data = NULL;
    node_arr_ptr->length = 0;
}

NodeArray LoadNodes(const char* filename, const U32 node_count)
{
    NodeArray nodes = NodeArrayCreate(node_count);

    FILE *file_ptr;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    file_ptr = fopen(filename, "r");
    if (file_ptr == NULL)
    {
        NodeArrayDestroy(&nodes);
        return nodes;
    }

    char start_section = 0;
    U32 node_idx = 0;

    while ((read = getline(&line, &len, file_ptr)) != -1)
    {
        if (strcmp("NODE_COORD_SECTION\n", line) == 0)
        {
            start_section = 1;
            continue;
        }

        if (strcmp("EOF\n", line) == 0)
            return nodes;

        if (start_section == 1)
        {
            char* token = strtok(line, " \n");
            int placement = 0;
            while (token)
            {
                switch(placement++)
                {
                    case 0:
                        nodes.data[node_idx].id = atoi(token);
                        break;
                    case 1:
                        nodes.data[node_idx].x = atoi(token);
                        break;
                    case 2:
                        nodes.data[node_idx].y = atoi(token);
                        break;
                }

                token = strtok(NULL, " \n");
            }

            node_idx++;
        }
    }

    free(line);
}

S32 Distance(const Node* n0_ptr, const Node* n1_ptr)
{
    F32 diffx = n0_ptr->x - n1_ptr->x;
    F32 diffy = n0_ptr->y - n1_ptr->y;
    return nearbyintf(sqrtf(diffx * diffx + diffy * diffy));
}

TrafficMtx TrafficMtxCreate(const U32 row_count, const U32 col_count)
{
    TrafficMtx mtx;
    mtx.row_count = row_count;
    mtx.col_count = col_count;
    mtx.data = (F32*)malloc(sizeof(F32) * row_count * col_count);

    return mtx;
}

void TrafficMtxDestroy(TrafficMtx* mtx_ptr)
{
    mtx_ptr->row_count = 0;
    mtx_ptr->col_count = 0;
    free(mtx_ptr->data);
    mtx_ptr->data = NULL;
}

void TrafficMtxUpdate(TrafficMtx* mtx_ptr, const F32 traffic_add_prob)
{
    for (U32 i = 0; i < mtx_ptr->row_count; i++)
    for (U32 j = i + 1; j < mtx_ptr->col_count; j++)
    {
        F32 u = RAND_F32();

        if (u < traffic_add_prob)
        {
            TrafficMtxSet(mtx_ptr, i, j, RAND_F32() * 5.0f);
            TrafficMtxSet(mtx_ptr, j, i, RAND_F32() * 5.0f);
        }
        else
        {
            TrafficMtxSet(mtx_ptr, i, j, 1.0f);
            TrafficMtxSet(mtx_ptr, j, i, 1.0f);
        }
    }
}

S32 Fitness(const NodeArray chr)
{
    S32 total_distance = 0;

    for (S32 i = 0; i < chr.length - 1; i++)
    {
        total_distance += Distance(&chr.data[i], &chr.data[i + 1]);
    }

    total_distance += Distance(&chr.data[0], &chr.data[chr.length - 1]);

    return -total_distance;
}

S32 FitnessWeighted(const NodeArray chr, const TrafficMtx* traffic_ptr)
{
    S32 total_distance = 0;

    for (S32 i = 0; i < chr.length - 1; i++)
    {
        Node node_a = chr.data[i];
        Node node_b = chr.data[i + 1];
        total_distance += Distance(&node_a, &node_b) * nearbyintf(TrafficMtxGet(*traffic_ptr, node_a.id - 1, node_b.id - 1));
    }

    Node node_a = chr.data[0];
    Node node_b = chr.data[chr.length - 1];
    total_distance += Distance(&node_a, &node_b) * nearbyintf(TrafficMtxGet(*traffic_ptr, node_a.id - 1, node_b.id - 1));

    return -total_distance;
}
