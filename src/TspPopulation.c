
#include "TspPopulation.h"

Population PopulationAlloc(const U32 size, const NodeArray nodes)
{
    Population population;
    population.chromosomes = (NodeArray*)malloc(sizeof(NodeArray) * size);
    population.size = 0;
    population.capacity = size;

    for (U32 i = 0; i < population.capacity; i++)
    {
        population.chromosomes[i] = NodeArrayCreate(nodes.length);
    }

    return population;
}

static Node* g_compare_node_ptr;

int CompareNn(const void* a, const void* b)
{
    U32 dist_to_a = Distance((Node*)a, g_compare_node_ptr);
    U32 dist_to_b = Distance((Node*)b, g_compare_node_ptr);

    if (dist_to_a < dist_to_b) return -1;
    if (dist_to_a == dist_to_b) return 0;
    if (dist_to_a > dist_to_b) return 1;
}

Population PopulationCreate(const U32 size, const NodeArray nodes)
{
    Population population;
    population.chromosomes = (NodeArray*)malloc(sizeof(NodeArray) * size);
    population.size = size;
    population.capacity = size;

    for (U32 i = 0; i < population.size; i++)
    {
        population.chromosomes[i] = NodeArrayCreate(nodes.length);
    }

    const U32 rand_chromosome_count = (U32)((F32)population.size * 1.0f);
    const U32 nn_chromosome_count = population.size - rand_chromosome_count;

    U32* shuffle = (U32*)malloc(sizeof(U32) * nodes.length);

    /* Init by random shuffling */
    for (U32 i = 0; i < rand_chromosome_count; i++)
    {
        for (U32 j = 0; j < nodes.length; j++)
        {
            shuffle[j] = j;
        }

        for (S32 j = nodes.length - 1; j >= 0; --j)
        {
            U32 rand_idx = rand() % (j + 1);
            SWAP(shuffle[j], shuffle[rand_idx], U32);
        }

        Node* chr_data = population.chromosomes[i].data;

        for (U32 j = 0; j < nodes.length; j++)
        {
            chr_data[j] = nodes.data[shuffle[j]];
        }
    }

    free(shuffle);

    /* Init by nearest-neighbor scheme */
    U8* selected_map = (U8*)calloc(nodes.length, sizeof(U8));
    NodeArray tmp_nodes = NodeArrayCreate(nodes.length);

    for (U32 i = rand_chromosome_count; i < population.size; i++)
    {
        NodeArrayCopy(&tmp_nodes, nodes);
        U32 rand_idx = rand() % tmp_nodes.length;

        while (selected_map[rand_idx] == 1)
        {
            rand_idx = (U32) rand() % tmp_nodes.length;
        }

        selected_map[rand_idx] = 1;

        SWAP(tmp_nodes.data[0], tmp_nodes.data[rand_idx], Node);

        Node* chr_data = population.chromosomes[i].data;
        chr_data[0] = tmp_nodes.data[0];

        for (U32 j = 1; j < tmp_nodes.length; j++)
        {
            g_compare_node_ptr = tmp_nodes.data + j - 1;
            qsort(tmp_nodes.data + j, tmp_nodes.length - j, sizeof(Node), CompareNn);
            g_compare_node_ptr = NULL;
            chr_data[j] = tmp_nodes.data[j];
        }
    }

    free(selected_map);
    NodeArrayDestroy(&tmp_nodes);

    return population;
}

void PopulationDestroy(Population* population_ptr)
{
    for (U32 i = 0; i < population_ptr->size; i++)
    {
        NodeArrayDestroy(&population_ptr->chromosomes[i]);
    }

    free(population_ptr->chromosomes);
    population_ptr->chromosomes = NULL;
    population_ptr->size = 0;
    population_ptr->capacity = 0;
}

static const TrafficMtx* g_traffic_mtx_ptr;

int QsortCompareFitnessDescending(const void* a, const void* b)
{
    S32 fitness_a = FitnessWeighted(*(NodeArray*)a, g_traffic_mtx_ptr);
    S32 fitness_b = FitnessWeighted(*(NodeArray*)b, g_traffic_mtx_ptr);

    if (fitness_a < fitness_b) return 1;
    if (fitness_a == fitness_b) return 0;
    if (fitness_a > fitness_b) return -1;
}

void PopulationSortByFitnessDescending(Population* population_ptr, const TrafficMtx* traffic_ptr)
{
    g_traffic_mtx_ptr = traffic_ptr;
    qsort(population_ptr->chromosomes, population_ptr->size, sizeof(NodeArray), QsortCompareFitnessDescending);
}

U32 PopulationGetBestIdx(const Population population, const TrafficMtx* traffic_ptr)
{
    U32 best_idx = UINT32_MAX;
    S32 best_fitness = INT32_MIN;

    for (U32 i = 0; i < population.size; i++)
    {
        S32 fitness;

        if (!traffic_ptr)
        {
            fitness = Fitness(population.chromosomes[i]);
        }
        else
        {
            fitness = FitnessWeighted(population.chromosomes[i], traffic_ptr);
        }

        if (fitness > best_fitness)
        {
            best_idx = i;
            best_fitness = fitness;
        }
    }

    return best_idx;
}

U32 PopulationGetWorstIdx(const Population population, const TrafficMtx* traffic_ptr)
{
    U32 worst_idx = UINT32_MAX;
    S32 worst_fitness = INT32_MAX;

    for (U32 i = 0; i < population.size; i++)
    {
        S32 fitness;

        if (!traffic_ptr)
        {
            fitness = Fitness(population.chromosomes[i]);
        }
        else
        {
            fitness = FitnessWeighted(population.chromosomes[i], traffic_ptr);
        }

        if (fitness < worst_fitness)
        {
            worst_idx = i;
            worst_fitness = fitness;
        }
    }

    return worst_idx;
}

F32 PopulationComputeAvgFitness(const Population population, const TrafficMtx* traffic_ptr)
{
    F32 weight = 1.0f / (F32)population.size;
    F32 avg = 0.0f;

    for (U32 i = 0; i < population.size; i++)
    {
        S32 fitness;

        if (!traffic_ptr)
        {
            fitness = Fitness(population.chromosomes[i]);
        }
        else
        {
            fitness = FitnessWeighted(population.chromosomes[i], traffic_ptr);
        }

        avg += weight * (F32)fitness;
    }

    return avg;
}

U32 PopulationSampleIdxTournament(const Population population, const U32 tournament_size, const TrafficMtx* traffic_ptr)
{
    U8* selected_map = (U8*)calloc(population.size, sizeof(U8));
    U32* sample_idx_buffer = (U32*)malloc(sizeof(U32) * population.size);

    for (U32 i = 0; i < tournament_size; i++)
    {
        U32 sample_idx = rand() % population.size;

        while (selected_map[sample_idx] == 1)
        {
            sample_idx = rand() % population.size;
        }

        sample_idx_buffer[i] = sample_idx;
        selected_map[sample_idx] = 1;
    }

    free(selected_map);

    U32 best_idx = UINT32_MAX;
    S32 best_fitness = INT32_MIN;

    for (U32 i = 0; i < tournament_size; i++)
    {
        U32 sample_idx = sample_idx_buffer[i];
        S32 sample_fitness;

        if (!traffic_ptr)
        {
            sample_fitness = Fitness(population.chromosomes[sample_idx]);
        }
        else
        {
            sample_fitness = FitnessWeighted(population.chromosomes[sample_idx], traffic_ptr);
        }

        if (sample_fitness > best_fitness)
        {
            best_fitness = sample_fitness;
            best_idx = sample_idx;
        }
    }

    free(sample_idx_buffer);

    return best_idx;
}

U32 PopulationSampleIdxMinDist(const Population population, const TrafficMtx* traffic_ptr)
{
    U32 pair[2] = { UINT32_MAX, UINT32_MAX };
    S32 min_dist = INT32_MAX;

    for (U32 i = 0; i < population.size; i++)
    for (U32 j = i + 1; j < population.size; j++)
    {
        S32 dist = abs(FitnessWeighted(population.chromosomes[i], traffic_ptr) - FitnessWeighted(population.chromosomes[j], traffic_ptr));

        if (dist < min_dist)
        {
            min_dist = dist;
            pair[0] = i;
            pair[1] = j;
        }
    }

    S32 fitness0 = FitnessWeighted(population.chromosomes[pair[0]], traffic_ptr);
    S32 fitness1 = FitnessWeighted(population.chromosomes[pair[1]], traffic_ptr);

    if (fitness0 < fitness1)
    {
        return pair[0];
    }
    else
    {
        return pair[1];
    }
}
