
#include "TspSolverGaDyna.h"

void DynaPolicySteadyState(const DynaPolicySettings settings, const NodeArray nodes, Population* population_ptr)
{
    const U32 population_size = population_ptr->size;
    PopulationDestroy(population_ptr);
    *population_ptr = PopulationCreate(population_size, nodes);
}

void DynaPolicyRandomImmigrants(const DynaPolicySettings settings, const NodeArray nodes, Population* population_ptr)
{
    const U32 immigrant_count = settings.replacement_rate;

    U8* selected_map = (U8*)calloc(population_ptr->size, sizeof(U8));
    U32* sample_idx_buffer = (U32*)malloc(sizeof(U32) * immigrant_count);

    for (U32 i = 0; i < immigrant_count; i++)
    {
        U32 sample_idx = rand() % population_ptr->size;

        while (selected_map[sample_idx] == 1)
        {
            sample_idx = rand() % population_ptr->size;
        }

        sample_idx_buffer[i] = sample_idx;
        selected_map[sample_idx] = 1;
    }

    free(selected_map);

    Population immigrant_population = PopulationCreate(immigrant_count, nodes);

    for (U32 i = 0; i < immigrant_population.size; i++)
    {
        NodeArrayCopy(&population_ptr->chromosomes[sample_idx_buffer[i]], immigrant_population.chromosomes[i]);
    }

    free(sample_idx_buffer);
    PopulationDestroy(&immigrant_population);
}

void EvolvePopulation(Population population, const TspSolverGaDynaSettings* settings_ptr, const NodeArray nodes, const TrafficMtx traffic_mtx)
{
    U32 parent0_idx = PopulationSampleIdxTournament(population, settings_ptr->tournament_size, &traffic_mtx);
    U32 parent1_idx = PopulationSampleIdxTournament(population, settings_ptr->tournament_size, &traffic_mtx);

    while (parent0_idx == parent1_idx)
    {
        parent1_idx = PopulationSampleIdxTournament(population, settings_ptr->tournament_size, &traffic_mtx);
    }

    const Chromosome parent0 = population.chromosomes[parent0_idx];
    const Chromosome parent1 = population.chromosomes[parent1_idx];

    Chromosome offspring0 = NodeArrayCreate(nodes.length);
    Chromosome offspring1 = NodeArrayCreate(nodes.length);
    (*settings_ptr->crossover_fn)(parent0, parent1, offspring0, offspring1);

    if (RAND_F32() < settings_ptr->mutation_prob)
    {
        (*settings_ptr->mutate_fn)(&offspring0);
        (*settings_ptr->mutate_fn)(&offspring1);
    }

    const U32 worst_chr_idx_1 = PopulationGetWorstIdx(population, &traffic_mtx);
    NodeArrayCopy(&population.chromosomes[worst_chr_idx_1], offspring0);

    const U32 worst_chr_idx_2 = PopulationGetWorstIdx(population, &traffic_mtx);
    NodeArrayCopy(&population.chromosomes[worst_chr_idx_2], offspring1);

    NodeArrayDestroy(&offspring0);
    NodeArrayDestroy(&offspring1);
}

void SolveDynaTspGaSteadyState(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr)
{
    Population population = PopulationCreate(settings_ptr->population_size, nodes);
    TrafficMtx traffic_mtx = TrafficMtxCreate(nodes.length, nodes.length);
    TrafficMtxUpdate(&traffic_mtx, settings_ptr->traffic_add_prob);

    clock_t t_start = clock();

    const U32 period_count = settings_ptr->max_generation_count / settings_ptr->traffic_update_freq;
    S32* best_per_period = (S32*)malloc(sizeof(S32) * period_count);

    for (U32 i = 0; i < period_count; i++)
    {
        best_per_period[i] = INT32_MIN;
    }

    for (U32 i = 0; i < settings_ptr->max_generation_count; i++)
    {
        const U32 period_idx = i / settings_ptr->traffic_update_freq;
        bool env_updated = false;

        if (i % settings_ptr->traffic_update_freq == 0)
        {
            TrafficMtxUpdate(&traffic_mtx, settings_ptr->traffic_add_prob);
            env_updated = true;
        }

        if (env_updated)
        {
            DynaPolicySteadyState(settings_ptr->dyna_policy_settings, nodes, &population);
        }
        else
        {
            EvolvePopulation(population, settings_ptr, nodes, traffic_mtx);
        }

        U32 worst_idx = PopulationGetWorstIdx(population, &traffic_mtx);
        U32 best_idx = PopulationGetBestIdx(population, &traffic_mtx);
        statistics_ptr->best_result = FitnessWeighted(population.chromosomes[best_idx], &traffic_mtx);
        statistics_ptr->avg_result = PopulationComputeAvgFitness(population, &traffic_mtx);

        if (best_per_period[period_idx] < statistics_ptr->best_result)
        {
            best_per_period[period_idx] = statistics_ptr->best_result;
        }

        {
            F32 perf = 0.0f;

            for (U32 h = 0; h <= period_idx; h++)
            {
                perf += (F32)(best_per_period[h]);
            }

            perf /= (F32)(period_idx + 1);

            statistics_ptr->perf = fabs(perf);

            if (statistics_ptr->perf_buffer)
            {
                statistics_ptr->perf_buffer[i] = fabs(perf);
            }
        }

        if ((F32)(clock() - t_start) / CLOCKS_PER_SEC >= settings_ptr->max_runtime)
        {
            break;
        }
    }

    free(best_per_period);

    PopulationDestroy(&population);
    TrafficMtxDestroy(&traffic_mtx);
}

void SolveDynaTspGaRandomImmigrants(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr)
{
    Population population = PopulationCreate(settings_ptr->population_size, nodes);
    TrafficMtx traffic_mtx = TrafficMtxCreate(nodes.length, nodes.length);
    TrafficMtxUpdate(&traffic_mtx, settings_ptr->traffic_add_prob);

    clock_t t_start = clock();

    const U32 period_count = settings_ptr->max_generation_count / settings_ptr->traffic_update_freq;
    S32* best_per_period = (S32*)malloc(sizeof(S32) * period_count);

    for (U32 i = 0; i < period_count; i++)
    {
        best_per_period[i] = INT32_MIN;
    }

    for (U32 i = 0; i < settings_ptr->max_generation_count; i++)
    {
        const U32 period_idx = i / settings_ptr->traffic_update_freq;
        bool env_updated = false;

        if (i % settings_ptr->traffic_update_freq == 0)
        {
            TrafficMtxUpdate(&traffic_mtx, settings_ptr->traffic_add_prob);
            env_updated = true;
        }

        /* Introduce immigrants */
        DynaPolicyRandomImmigrants(settings_ptr->dyna_policy_settings, nodes, &population);

        EvolvePopulation(population, settings_ptr, nodes, traffic_mtx);

        U32 worst_idx = PopulationGetWorstIdx(population, &traffic_mtx);
        U32 best_idx = PopulationGetBestIdx(population, &traffic_mtx);
        statistics_ptr->best_result = FitnessWeighted(population.chromosomes[best_idx], &traffic_mtx);
        statistics_ptr->avg_result = PopulationComputeAvgFitness(population, &traffic_mtx);

        if (best_per_period[period_idx] < statistics_ptr->best_result)
        {
            best_per_period[period_idx] = statistics_ptr->best_result;
        }

        {
            F32 perf = 0.0f;

            for (U32 h = 0; h <= period_idx; h++)
            {
                perf += (F32)(best_per_period[h]);
            }

            perf /= (F32)(period_idx + 1);

            statistics_ptr->perf = fabs(perf);

            if (statistics_ptr->perf_buffer)
            {
                statistics_ptr->perf_buffer[i] = fabs(perf);
            }
        }

        if ((F32)(clock() - t_start) / CLOCKS_PER_SEC >= settings_ptr->max_runtime)
        {
            break;
        }
    }

    free(best_per_period);

    PopulationDestroy(&population);
    TrafficMtxDestroy(&traffic_mtx);
}

void SolveDynaTspGaMemorySearch(const NodeArray nodes, const TspSolverGaDynaSettings* settings_ptr, TspSolverGaDynaStatistics* statistics_ptr)
{
    Population memory_population = PopulationCreate(settings_ptr->population_size, nodes);
    Population search_population = PopulationCreate(settings_ptr->population_size, nodes);
    Population explicit_population = PopulationAlloc(settings_ptr->population_size, nodes);

    TrafficMtx traffic_mtx = TrafficMtxCreate(nodes.length, nodes.length);
    TrafficMtxUpdate(&traffic_mtx, settings_ptr->traffic_add_prob);

    clock_t t_start = clock();

    const U32 period_count = settings_ptr->max_generation_count / settings_ptr->traffic_update_freq;
    S32* best_per_period = (S32*)malloc(sizeof(S32) * period_count);

    for (U32 i = 0; i < period_count; i++)
    {
        best_per_period[i] = INT32_MIN;
    }

    for (U32 i = 0; i < settings_ptr->max_generation_count; i++)
    {
        const U32 period_idx = i / settings_ptr->traffic_update_freq;
        bool env_updated = false;

        if (i % settings_ptr->traffic_update_freq == 0)
        {
            TrafficMtxUpdate(&traffic_mtx, settings_ptr->traffic_add_prob);
            env_updated = true;
        }

        /* Update explicit memory */
        {
            U32 best_memory_idx = PopulationGetBestIdx(memory_population, &traffic_mtx);
            U32 best_search_idx = PopulationGetBestIdx(search_population, &traffic_mtx);
            NodeArray src_chromosome;

            if (FitnessWeighted(memory_population.chromosomes[best_memory_idx], &traffic_mtx) > FitnessWeighted(search_population.chromosomes[best_search_idx], &traffic_mtx))
            {
                src_chromosome = memory_population.chromosomes[best_memory_idx];
            }
            else
            {
                src_chromosome = search_population.chromosomes[best_search_idx];
            }

            if (PopulationIsFull(explicit_population))
            {
                U32 target_idx = PopulationSampleIdxMinDist(explicit_population, &traffic_mtx);

                if (FitnessWeighted(explicit_population.chromosomes[target_idx], &traffic_mtx) < FitnessWeighted(src_chromosome, &traffic_mtx))
                {
                    NodeArrayCopy(&explicit_population.chromosomes[target_idx], src_chromosome);
                }
            }
            else
            {
                U32 target_idx = explicit_population.size++;
                NodeArrayCopy(&explicit_population.chromosomes[target_idx], src_chromosome);
            }
        }

        if (env_updated)
        {
            /* Reinit search population */
            PopulationDestroy(&search_population);
            search_population = PopulationCreate(settings_ptr->population_size, nodes);

            /**
            * Merge explicit memory and memory population and
            * copy `settings_ptr->population_size` best individuals to memory population
            */
            {
                Population merged_population = PopulationAlloc(memory_population.size + explicit_population.size, nodes);

                for (U32 c = 0; c < explicit_population.size; c++)
                {
                    NodeArrayCopy(&merged_population.chromosomes[c], explicit_population.chromosomes[c]);
                }

                for (U32 c = 0; c < memory_population.size; c++)
                {
                    NodeArrayCopy(&merged_population.chromosomes[c + explicit_population.size], memory_population.chromosomes[c]);
                }

                PopulationSortByFitnessDescending(&merged_population, &traffic_mtx);

                for (U32 c = 0; c < memory_population.size; c++)
                {
                    NodeArrayCopy(&memory_population.chromosomes[c], merged_population.chromosomes[c]);
                }

                PopulationDestroy(&merged_population);
            }
        }
        else
        {
            EvolvePopulation(memory_population, settings_ptr, nodes, traffic_mtx);
            EvolvePopulation(search_population, settings_ptr, nodes, traffic_mtx);
        }

        S32 best0 = FitnessWeighted(memory_population.chromosomes[PopulationGetBestIdx(memory_population, &traffic_mtx)], &traffic_mtx);
        S32 best1 = FitnessWeighted(search_population.chromosomes[PopulationGetBestIdx(search_population, &traffic_mtx)], &traffic_mtx);
        statistics_ptr->best_result = best0 > best1 ? best0 : best1;

        if (best_per_period[period_idx] < statistics_ptr->best_result)
        {
            best_per_period[period_idx] = statistics_ptr->best_result;
        }

        {
            F32 perf = 0.0f;

            for (U32 h = 0; h <= period_idx; h++)
            {
                perf += (F32)(best_per_period[h]);
            }

            perf /= (F32)(period_idx + 1);

            statistics_ptr->perf = fabs(perf);

            if (statistics_ptr->perf_buffer)
            {
                statistics_ptr->perf_buffer[i] = fabs(perf);
            }
        }

        if ((F32)(clock() - t_start) / CLOCKS_PER_SEC >= settings_ptr->max_runtime)
        {
            break;
        }
    }

    free(best_per_period);

    PopulationDestroy(&memory_population);
    PopulationDestroy(&search_population);
    PopulationDestroy(&explicit_population);
    TrafficMtxDestroy(&traffic_mtx);
}
